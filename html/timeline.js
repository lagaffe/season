const Timeline = {
  data() {
    return {
      bRef: "https://vuejsexamples.com/a-simple-timeline-in-vuejs",
      pgTitle: saison,
      timeline: matchList
    }
  },
  events: {
    "timeline-delete-item": function (id) {
      this.timeline = _.remove(this.timeline, function (item) {
        return item.id != id;
      });
    }
  }
}
const app = Vue.createApp(Timeline)

app.component('timeline', {
  props: ['items'],
  template: '<ul class="timeline"><timeline-item v-for="item in items" v-bind:item="item" v-bind:key="item.id" ></timeline-item></ul>',
  events: {
    "delete-item": function () { return true; }
  }

})

app.component('timeline-item', {
  props: ['item'],
  template: '\
    <li class="timeline-item" v-bind:class="item.action_needed">\
     <div class="timeline-badge" v-bind:class="item.icon_status"><i v-bind:class="item.icon_class"></i></div>\
     <div class="timeline-panel" v-bind:class="item.element_status" v-bind:class="item.element_day_marker">\
        <div class="timeline-heading">\
          <a class="timeline-title" v-bind:href="item.url"><h4 class="timeline-title" v-bind:class="item.text_status">{{ item.title }}</h4></a>\
          <h5 class="timeline-title">{{ item.place }}</h5>\
          <div class="timeline-panel-controls">\
            <div class="controls"><a v-for="control in item.controls" is="timeline-control" :control="control"></a></div>\
            <div class="timestamp"><small class="">{{ item.created }}</small></div>\
          </div>\
        </div>\
        <div class="timeline-body">\
          <p class="player-title">players:</p>\
          <p class="player-list">{{ item.players }}</p>\
          <p v-if="item.substitute" class="player-title">substitute:</p>\
          <p v-if="item.substitute" class="player-list">{{ item.substitute }}</p>\
        </div>\
      </div>\
    </li>',
  methods: {
    delete: function () { this.$dispatch("timeline-delete-item", this.item.id); },
    edit: function () {}
  },

  events: {
    "timeline-delete": function () { this.delete(); },
    "timeline-edit": function () { this.edit(); }
  }

})

app.component("timeline-control", {
  template: '<a href="#" @click="handleClick"><i v-bind:class="control.icon_class"></i></a>',
  props: ["control"],
  methods: {
    handleClick: function () {
      if (this.control.method == "delete") {
        this.$dispatch("timeline-delete");
      } else if (this.control.method == "edit") {
        this.$dispatch("timeline-edit");
      } else {
        console.log("Unknown method " + this.control.method);
      }
    }
  }
});


app.mount('#app')
