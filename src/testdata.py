import datetime
import yaml
from yaml.loader import SafeLoader
import random
import copy

RESULTS=[]
players={}

def getDate(start,window):
  return datetime.datetime.now()


def parsePlayerList(rd):
  ret={}
  for player in rd:
    ret[player['username']]=player
  return ret


def parseMatchList(rd,playerList,maxDateWin):
  now=datetime.datetime.now()
  ret=[]
  for match in rd:
    match['date']=now+datetime.timedelta(days=random.randrange(1,maxDateWin))
    players=match.pop('players')
    for p in players['present']:
      pp=copy.deepcopy(playerList[p])
      pp.update(match)
      pp['choice']='yes'
      ret.append(pp)
    for p in players['absent']:
      pp=copy.deepcopy(playerList[p])
      pp.update(match)
      pp['choice']='no'
      ret.append(pp)
    for p in players['substitute']:
      pp=copy.deepcopy(playerList[p])
      pp.update(match)
      pp['choice']='substitute'
      ret.append(pp)
    
  return ret


try:
  with open('testdata.yml') as f:
    rawdata = yaml.load(f, Loader=SafeLoader)
    players=parsePlayerList(rawdata['players'])
    RESULTS=parseMatchList(rawdata['matchs'],players,90)
except FileNotFoundError:
  pass

