class Match:
  ID=0
  POLLID=0
  ROUND=''
  DESC=''
  LOC=''
  URL=''
  PRESENT_LIST=None
  ABSENT_LIST=None
  SUBSTITUTE_LIST=None
  ERROR=None

  def __str__(self):
    out="{}({})/{} at '{}' starts at {}".format(self.ROUND,self.ID,self.DESC,self.LOC,self.DATE)
    return out


class Player:
  ID=0
  USERNAME=''
  FIRSTNAME=''
  LASTNAME=''
  GENDER=''

  def __str__(self):
    return self.USERNAME
