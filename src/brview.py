import config
import db
import ic
import icexceptions
from jinja2 import Template,Environment,FileSystemLoader
import datetime
import logging


def renderData(htmldir,team,saison,matchList,capitain):
  e=Environment(loader=FileSystemLoader('./'))
  tmplData=e.get_template('data.js.j2')
  dataRendered=tmplData.render({
    'team': team.upper(),
    'saison': saison,
    'matchlist': matchList,
    'now': datetime.datetime.now(),
    'caplogin': capitain
  })

  with open(htmldir + '/data.js', 'w') as ptr:
    ptr.write(dataRendered)


def dictToArray(matchList,sorted):
  ret=[]

  def heapifyTree(root,szTree):
    l=(root*2)+1
    r=(root*2)+2

    if (l>szTree): # is a leaf
      return

    heapifyTree(l,szTree)
    ml=l
    if (r<=szTree):
      heapifyTree(r,szTree)
      if (ret[r].DATE > ret[l].DATE):
        ml=r

    if (ret[ml].DATE > ret[root].DATE):
      ret[ml],ret[root]=ret[root],ret[ml]
    

  def heapTreeSort(ret):
    max=len(ret)-1
    for newSz in range(max,0,-1): # it will stop at 1
      heapifyTree(0,newSz)
      ret[newSz],ret[0]=ret[0],ret[newSz] 


  for id,match in matchList.items():
    ret.append(match)
  if sorted:
    heapTreeSort(ret)
  return ret


def finalizeMatchList(matchList,baseURL,capitain):

  def setLink(match,baseURL):
    match.URL=baseURL+'/'+match.URL+'/'


  def splitRound(match):
    t=match.DESC.split('-',1)
    match.ROUND=t[0].strip()
    match.DESC=t[1].strip()


  def validateMatch(match,capitain):
    match.ERROR=[]
    men=0
    women=0
    for p in match.PRESENT_LIST:
      if p.GENDER=='M':
        men+=1
      elif p.GENDER=='F':
        women+=1
    if men < 3:
      match.ERROR.append('not enough men ({})'.format(men))
    if women < 2:
      match.ERROR.append('not enough women ({})'.format(women))


  for id,match in matchList.items():
    splitRound(match)
    setLink(match,baseURL)
    validateMatch(match,capitain)


def removeRejectedMatch(matchList,caplogin):
  rm=[]
  for id,match in matchList.items():
    if len(match.ABSENT_LIST) > 0:
      if match.ABSENT_LIST[0].USERNAME == caplogin:
        rm.append(id)
  for id in rm:
    del(matchList[id])


def popNoCaptainVote(matchList,login):
  ret=[]
  for id,match in matchList.items():
    if len(match.ABSENT_LIST) == 0 or match.ABSENT_LIST[0].USERNAME != login:
      if len(match.PRESENT_LIST) == 0 or match.PRESENT_LIST[0].USERNAME != login:
        if len(match.SUBSTITUTE_LIST) == 0 or match.SUBSTITUTE_LIST[0].USERNAME != login:
          ret.append(match)
  for m in ret:
    del(matchList[m.ID])
  return ret
      

    
def setCaptainAsFirstVote(matchList,login):
  for id, match in matchList.items():
    voteNum=len(match.PRESENT_LIST)
    for i in range(0,voteNum):
      if match.PRESENT_LIST[i].USERNAME == login:
        if i > 0:
          capvote=match.PRESENT_LIST.pop(i)
          match.PRESENT_LIST.insert(0,capvote)
        break
    voteNum=len(match.SUBSTITUTE_LIST)
    for i in range(0,voteNum):
      if match.SUBSTITUTE_LIST[i].USERNAME == login:
        if i > 0:
          capvote=match.SUBSTITUTE_LIST.pop(i)
          match.SUBSTITUTE_LIST.insert(0,capvote)
        break
    voteNum=len(match.ABSENT_LIST)
    for i in range(0,voteNum):
      if match.ABSENT_LIST[i].USERNAME == login:
        if i > 0:
          capvote=match.ABSENT_LIST.pop(i)
          match.ABSENT_LIST.insert(0,capvote)
        break


if __name__ == '__main__':
#  logging.basicConfig(level=logging.INFO)
  db=db.Factory().getDB(config.DB_PARAM['type'])
  db.setTags(present=config.ANSWER_LABEL['present'],absent=config.ANSWER_LABEL['absent'],substitute=config.ANSWER_LABEL['substitute'])
  db.connect(config.DB_PARAM)
  matchList=db.getMatchList(groupname='bcbe1')
  db.close()

  setCaptainAsFirstVote(matchList,config.TEAMS['bcbe1']['bitpoll_cap_login'])
  removeRejectedMatch(matchList,config.TEAMS['bcbe1']['bitpoll_cap_login'])
  finalizeMatchList(matchList,config.BITPOLL_BASE_POLL_URL,config.TEAMS['bcbe1']['bitpoll_cap_login'])
  nocapvote=popNoCaptainVote(matchList,config.TEAMS['bcbe1']['bitpoll_cap_login'])
  finalList=dictToArray(matchList,True)

#  print("match list:")
#  for match in finalList:
#    print("# {}".format(match))
#    pres=[]
#    abs=[]
#    sub=[]
#    for pl in match.PRESENT_LIST:
#      pres.append(pl.USERNAME)
#    for pl in match.ABSENT_LIST:
#      abs.append(pl.USERNAME)
#    for pl in match.SUBSTITUTE_LIST:
#      sub.append(pl.USERNAME)
#    print(" * present: {}".format(','.join(pres)))
#    print(" * substitute: {}".format(','.join(sub)))
#    print(" * absent: {}".format(','.join(abs)))

  print("match without captain vote:")
  for match in nocapvote:
    print(match)

  renderData(config.HTMLDIR,'bcbe1',config.BADMINTON_SAISON,finalList,config.TEAMS['bcbe1']['bitpoll_cap_login'])
