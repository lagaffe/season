import pymysql
import icexceptions
import ic
import os
import logging
import testdata


class Factory:
  def getDB(self,dbtype):
    if dbtype == 'mysql':
      logging.debug("creating mysql object")
      return MySQL()
    elif dbtype == 'mymock':
      logging.debug("creating mysqlmock object")
      return MySQLMOCK()
    else:
      raise NotImplementedError("Database type '{}' not implemented yet".format(type))



class DB:
  _tag={
    'present': 'present',
    'absent': 'absent',
    'substitute': 'substitute',
  }
  def connect(self,param):
    raise NotImplementedError()
  def close(self):
    raise NotImplementedError()
  def getMatchList(self):
    raise NotImplementedError()

  def setTags(self,present=None,absent=None,substitute=None):
    if present:
      self._tag['present']=present
    if absent:
      self._tag['absent']=absent
    if substitute:
      self._tag['substitute']=substitute



class MySQL(DB):
  _connection=None
  _matchList={}
  _playerList={}


  def connect(self,param):
    self._connection=pymysql.connect(host=param['host'], port=param['port'], user=param['user'],password=param['passwd'], database=param['db'], cursorclass=pymysql.cursors.DictCursor)
    logging.debug("connected")


  def close(self):
    pass


  def _getMatch(self,id):
    if id in self._matchList.keys():
      return self._matchList[id]
    raise icexceptions.ICNotFound()


  def _getPlayer(self,id):
    if id in self._playerList.keys():
      return self._playerList[id]
    raise icexceptions.PlayerNotFound()


  def _createMatch(self,ln):
    m=ic.Match()
    m.ID=ln['choiceid']
    m.POLLID=ln['pollid']
    m.DESC=ln['poll']
    m.LOC=ln['addr']
    m.URL=ln['url']
    m.DATE=None
    m.PRESENT_LIST=[]
    m.ABSENT_LIST=[]
    m.SUBSTITUTE_LIST=[]
    return m


  def _createPlayer(self,ln):
    p=ic.Player()
    p.ID=ln['userid']
    p.USERNAME=ln['username']
    p.FIRSTNAME=ln['firstname']
    p.LASTNAME=ln['lastname']
    p.GENDER=ln['gender']
    return p


  def _translateToMatch(self,ln):
    try:
      p=self._getPlayer(ln['userid'])
    except icexceptions.PlayerNotFound:
      p=self._createPlayer(ln)
      self._playerList[ln['userid']]=p

    try:
      m=self._getMatch(ln['choiceid'])
    except icexceptions.ICNotFound:
      m=self._createMatch(ln)
      m.DATE=ln['date']
      self._matchList[ln['choiceid']]=m
    finally:
      if ln['choice'] == self._tag['present']:
        m.PRESENT_LIST.append(p)
      elif ln['choice'] == self._tag['absent']:
        m.ABSENT_LIST.append(p)
      elif ln['choice'] == self._tag['substitute']:
        m.SUBSTITUTE_LIST.append(p)


  def getMatchList(self,groupname=None):
    sql='SELECT pollid, choiceid, poll, addr, url, date, userid, username, firstname, lastname, gender, choice FROM vw_bcbe';
    if groupname:
      sql="{} WHERE grp='{}'".format(sql,groupname)
    with self._connection.cursor() as cursor:
      cursor.execute(sql)
      result = cursor.fetchall()
      for ln in result:
        self._translateToMatch(ln)
    return self._matchList


class MySQLMOCK(MySQL):
  def connect(self,param):
    pass

  def close(self):
    pass


  def getMatchList(self,groupname=None):
    for ln in testdata.RESULTS:
      self._translateToMatch(ln)
    return self._matchList
