class ICException(Exception):
  pass

class ICNotFound(ICException):
  pass

class PlayerNotFound(ICException):
  pass

class ChoiceNotFound(ICException):
  pass
