# Season
Summarize bitpolls poll into 1 dashboard. The current use-case is for a sport team ; each match is represented with 1 poll. Each match will be display in 1 card which will be ordered by date

# technologies used
* python
* mysql-python for the binding to the mysql database of bitpoll
* vue.js for the display
* https://vuejsexamples.com/a-multi-item-card-carousel-in-vue/
