USE bcbe;

CREATE OR REPLACE VIEW vw_bcbe AS 
SELECT poll.id as pollid,poll.title as poll,poll.description as addr,poll.url as url,
       choice.id as choiceid, CONVERT_TZ(choice.date,'UTC',poll.timezone_name) as date,
       user.id as userid, user.username,user.first_name as firstname,user.last_name as lastname, gen.gender as gender,
       val.title as choice, grp.name as grp
FROM poll_poll as poll,
     poll_vote as pv,
     base_bitpolluser as user,
     gender as gen,
     base_bitpolluser_groups ug,
     auth_group as grp,
     poll_choice as choice,
     poll_votechoice as vc,
     poll_choicevalue val
WHERE pv.poll_id=poll.id
  AND pv.user_id=user.id
  AND choice.poll_id=poll.id
  AND choice.id=vc.choice_id
  AND vc.value_id=val.id
  AND vc.vote_id=pv.id
  AND user.id = gen.user_id
  AND user.id = ug.bitpolluser_id
  AND ug.group_id = grp.id
  AND grp.name LIKE "bcbe%"
;
